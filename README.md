# Varbase FAQS

This is a [Varbase](https://www.drupal.org/project/varbase) Feature.
Provides FAQ content type and related configuration. A frequently asked
 question and its answer.

Based on [FAQ](https://www.drupal.org/project/faq) module

## [Varbase documentation](https://docs.varbase.vardot.com/v/10.0.x/developers/understanding-varbase/external-components/varbase-faqs)
Check out Varbase documentation for more details.

* [Varbase FAQs Module](https://docs.varbase.vardot.com/v/10.0.x/developers/understanding-varbase/external-components/varbase-faqs)


Join Our Slack Team for Feedback and Support
http://slack.varbase.vardot.com/

This module is sponsored and developed by [Vardot](https://www.drupal.org/vardot).
